﻿using UnityEngine;
using System.Collections;

public class CollisionDetection : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.collider.tag == "Bucket") 
		{
			CreateObjects createObjScript=Camera.main.GetComponent<CreateObjects>();
			ClownMove clownMoveScript=collision.gameObject.GetComponent<ClownMove>();

			if (this.gameObject.tag=="FireBall")
			{
				createObjScript.UpdateObjectGenerationTime(0.5);
			}
			else if (this.gameObject.tag=="CalmBall")
			{
				createObjScript.UpdateObjectGenerationTime(2);
			}
			else if (this.gameObject.tag=="BonusPointsBall")
			{
				clownMoveScript.UpdateScore(10);
			}
			Destroy(this.gameObject);

			clownMoveScript.UpdateScore(1);
		}
		if (collision.collider.tag == "Ground") 
		{
			ClownMove clownMoveScript=collision.gameObject.GetComponent<ClownMove>();
			clownMoveScript.UpdateBallsDropped();
			Destroy(this.gameObject);
		}
	}

}
