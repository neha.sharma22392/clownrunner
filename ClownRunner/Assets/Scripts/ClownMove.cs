﻿using UnityEngine;
using System.Collections.Generic;

public class ClownMove : MonoBehaviour {

	public Vector3 speed=new Vector3(10,0,0);

	//private List <GameObject> ballsDropped=new List<GameObject>();
	public GameObject ballTemplate;
	public GUISkin scoreSkin;

	private int nLives=5;
	// Use this for initialization
	void Start () {
		PlayerPrefs.SetInt("score", 0);
		PlayerPrefs.SetInt("ballsDropped", 0);
	}
	
	// Update is called once per frame
	void Update () {
		float inputX=Input.GetAxis("Horizontal");
		this.rigidbody2D.velocity =new Vector2(inputX * speed.x,0);

		//this.transform.position = transform.position + speed;

	}
	public void UpdateScore(int incremenyBy)
	{
		PlayerPrefs.SetInt("score", PlayerPrefs.GetInt ("score") + incremenyBy);
	}
	public void UpdateBallsDropped()
	{
		PlayerPrefs.SetInt("ballsDropped", PlayerPrefs.GetInt ("ballsDropped") + 1);

		if (PlayerPrefs.GetInt ("ballsDropped") <= nLives) 
		{
			GameObject obj = Instantiate (ballTemplate) as GameObject;
			obj.rigidbody2D.gravityScale = 0;
			obj.rigidbody2D.mass = 0;
			float xPos =PlayerPrefs.GetInt ("ballsDropped") * (float)0.5;
			obj.transform.position = new Vector2(-(float)11.5+xPos,(float)5.5); 
		}
		else 
		{
			GameOver();
		}

	}
	void OnGUI()
	{
		GUI.skin = scoreSkin;
		GUI.Label(new Rect(5, 10, 250, 50), "Score: "+PlayerPrefs.GetInt ("score"));
		//GUI.Label(new Rect(10, 40, 100, 90), "Balls Dropped: "+PlayerPrefs.GetInt ("ballsDropped"));
	}
	void GameOver()
	{
		Application.LoadLevel("FinalScene");
	}
}
