﻿using UnityEngine;
using System.Collections;

public class CreateObjects : MonoBehaviour {

	private GameObject objectTemplate;
	private float timeElapsed=0;
	private double objectGenerationTime=2;
	public GameObject [] ballsTemplate;

	// Use this for initialization
	void Start () {
		StartCoroutine("SpawnObjects");
	}
	
	// Update is called once per frame
	void Update () 
	{
		timeElapsed += Time.deltaTime;
		if (timeElapsed>objectGenerationTime)
		{
			SpawnObjects();
			timeElapsed=0;
		}
	}
	public void UpdateObjectGenerationTime(double newTime)
	{
		objectGenerationTime = newTime;
	}
	void SpawnObjects()
	{
		int randomN = Random.Range (0, ballsTemplate.Length);
		if (randomN == 0) 
		{
			int coinToss = Random.Range (0, 1);
			//Generate bonus ball only if we land on 0 again (Heads on coin toss)
			if (coinToss == 0) {
				Debug.Log("Now generate");
				objectTemplate = ballsTemplate [randomN];
				
				GameObject obj = Instantiate (objectTemplate) as GameObject;
				obj.transform.position = new Vector2 (Random.Range (-2, 2), 10);
			}
		}
		else 
		{
			objectTemplate = ballsTemplate [randomN];

			GameObject obj = Instantiate (objectTemplate) as GameObject;
			obj.transform.position = new Vector2 (Random.Range (-2, 2), 10);
		}
	}
}
